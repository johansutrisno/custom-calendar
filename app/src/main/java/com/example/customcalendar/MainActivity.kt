package com.example.customcalendar

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.applandeo.materialcalendarview.EventDay
import com.example.customcalendar.Helper.formatDate
import com.example.customcalendar.Helper.formatDay
import com.example.customcalendar.Helper.formatMonth
import com.example.customcalendar.Helper.formatYear
import com.example.customcalendar.Helper.getTime
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupMaterialCalendar()
        setEvent()
        setHighlighted()
    }

    private fun setupMaterialCalendar() {
        calendarView.apply {
            initiateCalendar()
            setOnDayClickListener { onClickDay(it) }
        }
    }

    // untuk set tanggal hari ini
    private fun initiateCalendar() {
        val date = getTime()
        val day = formatDay(date)
        val month = formatMonth(date)
        val year = formatYear(date)

        val calendar = Calendar.getInstance()
        calendar.set(year, month, day)

        calendarView?.setDate(calendar)
    }

    // listener untuk tanggal
    private fun onClickDay(eventDay: EventDay) {
        val date = formatDate(eventDay.calendar.time)
        Toast.makeText(this, date, Toast.LENGTH_SHORT).show()
    }

    // contoh untuk highlighted
    private fun setHighlighted() {
        val calendars = listOf<Calendar>(
            Calendar.getInstance().apply { set(2020, 4, 15) },
            Calendar.getInstance().apply { set(2020, 4, 18) },
            Calendar.getInstance().apply { set(2020, 4, 27) }
        )

        calendarView?.setHighlightedDays(calendars)

    }

    // contoh untuk event
    private fun setEvent() {
        val events: ArrayList<EventDay> = ArrayList()

        val calendar1 = Calendar.getInstance()
        calendar1.set(2020, 4, 1)
        events.add(EventDay(calendar1, R.drawable.ic_dot))

        val calendar2 = Calendar.getInstance()
        calendar2.set(2020, 4, 4)
        events.add(EventDay(calendar2, R.drawable.ic_dot))

        val calendar3 = Calendar.getInstance()
        calendar3.set(2020, 4, 10)
        events.add(EventDay(calendar3, R.drawable.ic_dot))

        calendarView?.setEvents(events)
    }
}

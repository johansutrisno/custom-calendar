package com.example.customcalendar

import java.text.SimpleDateFormat
import java.util.*

object Helper {
    fun getTime(): Date {
        val c = Calendar.getInstance()
        c.timeZone = TimeZone.getTimeZone("GMT+07:00")
        return c.time
    }

    fun formatDate(date: Date): String {
        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale("id"))
        return formatter.format(date)
    }

    fun formatDay(date: Date): Int {
        val formatter = SimpleDateFormat("dd")
        return formatter.format(date).toInt()
    }

    fun formatMonth(date: Date): Int {
        val formatter = SimpleDateFormat("MM")
        return formatter.format(date).toInt()
    }

    fun formatYear(date: Date): Int {
        val formatter = SimpleDateFormat("yyyy")
        return formatter.format(date).toInt()
    }
}